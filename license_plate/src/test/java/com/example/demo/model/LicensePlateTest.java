package com.example.demo.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LicensePlateTest {

    @Test
    void testToString() {
        assertEquals("А000АА 116 RUS", new LicensePlate("А000АА").toString());
        assertEquals("А000АА 116 RUS", new LicensePlate((short) 0, "ААА").toString());
    }

    @Test
    void compareTo() {
        LicensePlate zero = new LicensePlate("А000АА");
        LicensePlate zero2 = new LicensePlate("А000АА");
        LicensePlate zeroB = new LicensePlate("А000АВ");
        LicensePlate one = new LicensePlate("А001АА");
        LicensePlate oneB = new LicensePlate("А001АВ");
        LicensePlate two = new LicensePlate("А002АА");

        assertEquals(0, zero.compareTo(zero2));
        assertEquals(0, zero.compareTo(zero));
        assertEquals(0, zero2.compareTo(zero));


        assertTrue(zero.compareTo(one) < 0);
        assertTrue(zero.compareTo(two) < 0);
        assertTrue( one.compareTo(two) < 0);

        assertTrue(zero.compareTo(two) < 0);
        assertTrue(zero.compareTo(zeroB) < 0);
        assertTrue(zeroB.compareTo(oneB) < 0);
        assertTrue(two.compareTo(zeroB) < 0);

        assertTrue(two.compareTo(zero) > 0);
        assertTrue(zeroB.compareTo(zero) > 0);
        assertTrue(oneB.compareTo(zeroB) > 0);
        assertTrue(zeroB.compareTo(two) > 0);
    }
}