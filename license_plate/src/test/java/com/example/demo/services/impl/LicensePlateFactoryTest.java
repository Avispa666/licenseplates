package com.example.demo.services.impl;

import com.example.demo.model.LicensePlate;
import com.example.demo.repositories.LicensePlateRepository;
import com.example.demo.services.LicensePlateService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import java.util.stream.Collectors;

class LicensePlateFactoryTest {
    private static LicensePlateRepository licensePlateRepository;
    private static LicensePlateService licensePlateService;

    @BeforeAll
    static void setUpClass() {
        licensePlateRepository = Mockito.mock(LicensePlateRepository.class);
        Set<LicensePlate> plates = new TreeSet<>();
        char[] letters = {'А', 'Е', 'Т', 'О', 'Р', 'Н', 'У', 'К', 'Х', 'С', 'В', 'М'};
        //fill with all 1 728 000 possible plates
        for(short i = 0; i < 1000; ++i) {
            for(char a : letters) {
                for (char b : letters) {
                    for (char c : letters) {
                        plates.add(new LicensePlate(i, "" + a + b + c));
                    }
                }
            }
        }
        plates.remove(new LicensePlate("С399ВА"));
        plates.remove(new LicensePlate("С400ВА"));
        plates.remove(new LicensePlate("С999ВА"));
        plates.remove(new LicensePlate("С999ВВ"));
        Mockito.when(licensePlateRepository.findAll()).thenReturn(new ArrayList<>(plates));
    }

    @BeforeEach
    void setUp() {
        licensePlateService = new LicensePlateFactory(licensePlateRepository);
    }

    @Test
    void getRandom() {
        List<String> free = new LinkedList<>();
        free.add(new LicensePlate("С399ВА").toString());
        free.add(new LicensePlate("С400ВА").toString());
        free.add(new LicensePlate("С999ВА").toString());
        free.add(new LicensePlate("С999ВВ").toString());
        assertTrue(free.contains(licensePlateService.getRandom()));
        assertTrue(free.contains(licensePlateService.getRandom()));
        assertTrue(free.contains(licensePlateService.getRandom()));
        assertTrue(free.contains(licensePlateService.getRandom()));
        assertFalse(free.contains(licensePlateService.getRandom()));
        assertEquals("", licensePlateService.getRandom());

    }

    @Test
    void getNext() {
        assertEquals(new LicensePlate("С399ВА").toString(), licensePlateService.getNext());
        assertEquals(new LicensePlate("С400ВА").toString(), licensePlateService.getNext());
        assertEquals(new LicensePlate("С999ВА").toString(), licensePlateService.getNext());
        assertEquals(new LicensePlate("С999ВВ").toString(), licensePlateService.getNext());
        assertEquals("", licensePlateService.getNext());
    }
}