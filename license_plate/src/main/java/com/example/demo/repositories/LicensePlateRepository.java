package com.example.demo.repositories;

import com.example.demo.model.LicensePlate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;

public interface LicensePlateRepository extends JpaRepository<LicensePlate, Integer> {
    @Async
    <S extends LicensePlate> S saveAndFlush(S s);
}
