package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class LicensePlate implements Comparable<LicensePlate>{
    private static final String REGION = " 116 RUS";
    @Setter
    @Getter
    private short digits;

    @Column(length = 3)
    @Setter
    @Getter
    private String letters;

    @Setter
    @Getter
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id; //integer is enough for all possible numbers (~2 millions)

    public LicensePlate() {
    }

    public LicensePlate(short digits, String letters) {
        this.digits = (short) (digits % 1000);
        this.letters = letters.toUpperCase();
    }

    public LicensePlate(String number) {
        number = number.toUpperCase();
        this.digits = Short.parseShort(number.substring(1,4));
        this.letters = number.charAt(0) + number.substring(4);
    }

    @Override
    public String toString() {
        return letters.charAt(0) + String.format("%03d", digits % 1000) + letters.substring(1) + REGION;
    }

    @Override
    public int compareTo(LicensePlate o) {
        int result = this.letters.compareTo(o.letters);
        if (result == 0) {
            if(this.digits % 1000 < o.digits % 1000) result = -1;
            if(this.digits % 1000 > o.digits % 1000) result = 1;
        }
        return result;
    }
}
