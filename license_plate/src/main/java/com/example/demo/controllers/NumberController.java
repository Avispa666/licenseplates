package com.example.demo.controllers;

import com.example.demo.services.LicensePlateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NumberController {
    private final LicensePlateService licensePlateService;

    @Autowired
    public NumberController(LicensePlateService lps) {this.licensePlateService = lps;}

    @GetMapping("/random")
    public String getRandom() {
        return licensePlateService.getRandom();
    }

    @GetMapping("/next")
    public String getNext() {
        return licensePlateService.getNext();
    }
}
