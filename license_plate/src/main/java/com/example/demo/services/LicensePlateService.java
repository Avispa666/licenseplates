package com.example.demo.services;

public interface LicensePlateService {
    public String getRandom();
    public String getNext();
}
