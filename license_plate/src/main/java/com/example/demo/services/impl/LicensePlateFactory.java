package com.example.demo.services.impl;

import com.example.demo.model.LicensePlate;
import com.example.demo.repositories.LicensePlateRepository;
import com.example.demo.services.LicensePlateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.TreeSet;

@Service
public class LicensePlateFactory  implements LicensePlateService {
    private static final Logger log = LoggerFactory.getLogger(LicensePlateFactory.class);

    private TreeSet<LicensePlate> plates;
    private LicensePlateRepository licensePlateRepository;

    @Autowired
    public LicensePlateFactory(LicensePlateRepository lpr) {
        this.licensePlateRepository = lpr;
        plates = new TreeSet<>();
        char[] letters = {'А', 'Е', 'Т', 'О', 'Р', 'Н', 'У', 'К', 'Х', 'С', 'В', 'М'};
        //fill with all 1 728 000 possible plates
        for(short i = 0; i < 1000; ++i) {
            for(char a : letters) {
                for (char b : letters) {
                    for (char c : letters) {
                        plates.add(new LicensePlate(i, "" + a + b + c));
                    }
                }
            }
        }
        log.debug("Set filled with all possible numbers");
        //remove already leased (from db)
        plates.removeAll(licensePlateRepository.findAll());
        log.debug("Already leased numbers removed");
    }

    public String getRandom() {
        long pos = (long) (plates.size() * Math.random());
        Iterator<LicensePlate> i = plates.iterator();
        while (i.hasNext()) {
            LicensePlate lp = i.next();
            if (pos-- == 0) {
                i.remove();
                log.debug("getRandom: " + lp.toString() + "; " + plates.size() + " numbers left");
                licensePlateRepository.saveAndFlush(lp);
                return lp.toString();
            }
        }
        log.debug("getRandom: all numbers are already leased");
        return "";
    }

    public String getNext() {
        LicensePlate lp = plates.pollFirst();
        if(lp == null) {
            log.debug("getNext: all numbers are already leased");
            return "";
        }
        else {
            licensePlateRepository.saveAndFlush(lp);
            log.debug("getNext: " + lp.toString() + "; " + plates.size() + " numbers left");
            return lp.toString();
        }
    }
}
